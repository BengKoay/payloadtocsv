const fs = require('fs-extra');
const moment = require('moment');
const ObjectsToCsv = require('objects-to-csv');
// const XLSX = require('xlsx');

//jshint esversion:8
//jshint node:true
// const fs = require('fs');
const path = require('path');

const moveFrom = '3393438d-1a47-4588-892e-625af031d3f4'; // read file from
const moveTo = 'new'; // create file to

const outputToExcel = [];
const pushInverter = [];
const excel = 'output.csv';

// const moveTo = '3393438d-1a47-4588-892e-625af031d3f4';
// const moveFrom = 'new';

// Make an async function that gets executed immediately
(async () => {
	// Our starting point
	try {
		//   let wb = XLSX.utils.book_new();
		// Get the files as an array
		const files = await fs.promises.readdir(moveFrom);

		// Loop them all with the new for...of
		for (const file of files) {
			// Get the full paths
			const fromPath = path.join(moveFrom, file);
			const toPath = path.join(moveTo, file);

			// Stat the file to see if we have a file or dir
			const stat = await fs.promises.stat(fromPath);

			if (stat.isFile()) console.log("'%s' is a file.", fromPath);
			else if (stat.isDirectory())
				console.log("'%s' is a directory.", fromPath);

			// // Now move async
			// await fs.promises.rename(fromPath, toPath);
			// console.log('fromPath', fromPath, file);
			const eachFile = await fs.promises.readdir(fromPath);
			// console.log('eachFile', eachFile);
			for (const readFromEachFile of eachFile) {
				const fromFilePath = path.join(fromPath, readFromEachFile);
				// console.log(fromFilePath);
				let readEachFile = fs.readJSONSync(fromFilePath);
				// console.log(readEachFile.info.createdAt);
				// console.log(
				// 	moment(readEachFile.info.createdAt).format('YYYY-MM-DD HH:mm')
				// );

				if (readEachFile.inverter.length > 0) {
					let payload = {
						Time: moment(readEachFile.info.createdAt).format(
							'YYYY-MM-DD HH:mm'
						),
					};
					for (const eachinverter in readEachFile.inverter) {
						// 'MPPT Power': readEachFile.inverter[0].mpptPower[0].power,
						// 'MPPT Voltage': readEachFile.inverter[0].mpptPower[0].voltage['1'],
						// 'MPPT Current': readEachFile.inverter[0].mpptPower[0].current['1'],

						payload = {
							...payload,
							[`Ref SN_${eachinverter}`]:
								readEachFile.inverter[`${eachinverter}`].refSn,
							[`Active Power_${eachinverter}`]:
								readEachFile.inverter[`${eachinverter}`].acPower,
						};
						// payload[`Ref SN_${eachinverter}`] =
						// 	readEachFile.inverter[`${eachinverter}`].refSn;

						// payload[`Active Power_${eachinverter}`] =
						// 	readEachFile.inverter[`${eachinverter}`].acPower;
						// console.log(readEachFile.inverter[eachinverter]);
						// got inverter data
					}
					outputToExcel.push(payload);
					// console.log(readEachFile.inverter);}
				}
				// let pushToExcel = {};
			}
			// console.log()

			// // Log because we're crazy
			// console.log("Moved '%s'->'%s'", fromPath, toPath);
		} // End for...of
		const csv = new ObjectsToCsv(outputToExcel);

		// Save to file:
		await csv.toDisk(`new/${excel}`);
	} catch (e) {
		// Catch anything bad that happens
		console.error("We've thrown! Whoops!", e);
	}
})(); // Wrap in parenthesis and call now
// const readAllFolder = (dirMain) => {
// 	const readDirMain = fs.readdirSync(dirMain);

// 	console.log(dirMain);
// 	console.log(readDirMain);

// 	readDirMain.forEach((dirNext) => {
// 		console.log(dirNext, fs.lstatSync(dirMain + '/' + dirNext).isDirectory());
// 		if (fs.lstatSync(dirMain + '/' + dirNext).isDirectory()) {
// 			readAllFolder(dirMain + '/' + dirNext);
// 		}
// 	});
// };

// const fs = require('fs');
// const util = require('util');
// const path = require('path');

// // Multiple folders list
// const in_dir_list = [
// 	'Folder 1 Large',
// 	'Folder 2 Small', // small folder and files will complete first
// 	'Folder 3 Extra Large',
// ];

// // BEST PRACTICES: (1) Faster folder list For loop has to be outside async_capture_callback functions for async to make sense
// //                 (2) Slower Read Write or I/O processes best be contained in an async_capture_callback functions because these processes are slower than for loop events and faster completed items get callback-ed out first

// for (i = 0; i < in_dir_list.length; i++) {
// 	var in_dir = in_dir_list[i];

// 	// function is created (see below) so each folder is processed asynchronously for readFile_async that follows
// 	readdir_async_capture(in_dir, function (files_path) {
// 		console.log('Processing folders asynchronously ...');

// 		for (j = 0; j < files_path.length; j++) {
// 			file_path = files_path[j];
// 			file = file_path.substr(file_path.lastIndexOf('/') + 1, file_path.length);

// 			// function is created (see below) so all files are read simultaneously but the smallest file will be completed first and get callback-ed first
// 			readFile_async_capture(file_path, file, function (file_string) {
// 				try {
// 					console.log(file_path);
// 					console.log(file_string);
// 				} catch (error) {
// 					console.log(error);
// 					console.log(
// 						'System exiting first to catch error if not async will continue...'
// 					);
// 					process.exit();
// 				}
// 			});
// 		}
// 	});
// }

// // fs.readdir async_capture function to deal with asynchronous code above
// function readdir_async_capture(in_dir, callback) {
// 	fs.readdir(in_dir, function (error, files) {
// 		if (error) {
// 			return console.log(error);
// 		}
// 		files_path = files.map(function (x) {
// 			return path.join(in_dir, x);
// 		});
// 		callback(files_path);
// 	});
// }

// // fs.readFile async_capture function to deal with asynchronous code above
// function readFile_async_capture(file_path, file, callback) {
// 	fs.readFile(file_path, function (error, data) {
// 		if (error) {
// 			return console.log(error);
// 		}
// 		file_string = data.toString();
// 		callback(file_string);
// 	});
// }
